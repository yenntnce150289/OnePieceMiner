using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    private LineRenderer lineRenderer;
    [SerializeField] private Transform start;
   // [SerializeField] private Transform[] points;

    Vector3 vStart;
    Vector3 vEnd;

    //private void Awake()
    //{
    //    lineRenderer = GetComponent<LineRenderer>();

    //}

    // Start is called before the first frame update
    void Start()
    {
        //lineRenderer.positionCount = points.Length;
        vStart = start.position; 
        //vEnd = end.position;
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0, vStart);
        lineRenderer.SetPosition(1, transform.position);
        // lineRenderer.SetPosition(1, vEnd);
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, start.position);
        //transform.position = new Vector3(transform.position.x, transform.position.y, 0);
       lineRenderer.SetPosition(1, transform.position);
        
    }
}

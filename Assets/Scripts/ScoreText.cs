using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    public Text changingText;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void setText (string _scoreText)
    {
        changingText.text = _scoreText;
    }

    public static implicit operator ScoreText(float v)
    {
        throw new NotImplementedException();
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager Instance;

    public AudioSource[] sfx;
    public AudioSource[] bgm;
    
    

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.Instance.PlayBGM(0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlaySFX(int soundToPlay)
    {
        //sfx[soundToPlay].pitch = Random.Range(0.9f, 1.1f);
        if (soundToPlay < sfx.Length)
        {

            sfx[soundToPlay].Play();
        }
    }
    public void StopSFX(int soundToStop)
    {
        if (soundToStop < sfx.Length)
        {
            sfx[soundToStop].Stop();
        }
    }

    public void PlayBGM(int musicToPlay)
    {
        StopBGM();
        if (musicToPlay < bgm.Length)
        {
            bgm[musicToPlay].Play();
        }
    }

    public void StopBGM()
    {
        for (int i = 0; i < bgm.Length; i++)
        {
            bgm[i].Stop();
        }
    }



}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeController : MonoBehaviour
{
    [SerializeField] private string audioParameter;
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private Slider slider;
    [SerializeField] private float valueMultiplier;
    // Start is called before the first frame update

    private void Awake()
    {
        slider.onValueChanged.AddListener(sliderValueController);
        slider.minValue = 0.0001f;
    }
    private void sliderValueController(float value)
    {
        audioMixer.SetFloat(audioParameter, Mathf.Log10(value) * valueMultiplier);
    }
}
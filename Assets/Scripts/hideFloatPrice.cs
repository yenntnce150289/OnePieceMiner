using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideFloatPrice : MonoBehaviour
{
    [SerializeField]private GameObject gameObject;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }
    private void Update()
    {
          Destroy(GameObject.Find("FloatingPrice(Clone)"), 0.45f);
            //StartCoroutine(DeleteAfterSeconds(2f, gameObject));
    }
    public void ShowPrice()
    {
        gameObject.SetActive(true);
    }

}

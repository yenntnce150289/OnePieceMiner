using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMovement : MonoBehaviour
{
    Rigidbody2D myBody;
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;

    public float left;
    public float right;

    [SerializeField]
    float speedMove;

    // Start is called before the first frame update
    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x; //extents = size of width / 2
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y; //extents = size of height / 2
        left = screenBounds.x * -1 + objectWidth + 0.2f ;
        right = screenBounds.x - objectWidth - 0.2f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        move();
    }
    private void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectHeight, screenBounds.y - objectHeight);
        transform.position = viewPos;
    }
    void move()
    {
        if ((myBody.position.x < left || myBody.position.x > right))
        {
            flip();
        }
        myBody.velocity = new Vector2(Time.deltaTime * -speedMove, 0);
    }

    void flip()
    {
        speedMove *= -1;
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
    }
}

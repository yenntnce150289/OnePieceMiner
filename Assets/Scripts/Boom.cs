using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class Boom : RodController
{
   private Animator _BoomAnimator;
    public GameObject boomObject;
    void Start()
    {
       _BoomAnimator = GetComponent<Animator>();
        _BoomAnimator.enabled = false;
    }
    public void Bang(Vector2 pos, bool flag = false)
    {
        _BoomAnimator.enabled = true;
        var hits = Physics2D.CircleCastAll(pos, 3, Vector2.zero);
        foreach (var hit in hits)
        {
            if (hit.collider == null)
            {
                continue;
            }
           else if (hit.transform.tag == Config.TAG_GOLD || hit.transform.tag == Config.TAG_DIAMOND || hit.transform.tag == Config.TAG_COIN || hit.transform.tag == Config.TAG_GARBAGE)
            {
                Destroy(hit.transform.gameObject);
            }
            else if (hit.transform.tag == Config.TAG_BOOM || hit.transform.tag == "boom" || hit.transform == boomObject)
            {
                hit.transform.tag = Config.TAG_GOLD;
                hit.transform.GetComponent<Boom>().Bang(hit.point);
            }
        }
    }
   
}



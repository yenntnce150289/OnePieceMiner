using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour
{
    // Start is called before the first frame update
    public const string TAG_GOLD = "gold";
    public const string TAG_BOOM = "boom";
    public const string TAG_DIAMOND = "diamond";
    public const string TAG_COIN = "coin";
    public const string TAG_GARBAGE = "garbage";
}

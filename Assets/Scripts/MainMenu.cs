using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject gamePlay;
    public void PlayGame()
    {
        gamePlay.SetActive(true);
        Time.timeScale = 0;
    }

    public void QuitGame() { 
         Application.Quit();
    }
}

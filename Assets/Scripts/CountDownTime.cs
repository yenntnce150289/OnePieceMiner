using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CountDownTime : MonoBehaviour
{
    public float timeStart;
    public TextMeshProUGUI textBox;
    public bool StartTime = false;
    // Use this for initialization
    void Start()
    {
        timeStart = 60;
        textBox.text = timeStart.ToString();
        StartTime = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (StartTime)
        {
            timeStart -= Time.deltaTime;
            textBox.text = Mathf.Round(timeStart).ToString();
            if (timeStart <= 0)
            {
                GameManager.Instance.gameLose();
            }
        }
    }
    public void ResetTime()
    {
        timeStart = 60;
        textBox.text = timeStart.ToString();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.SceneManagement;
using static PodController;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject menuGame;
    // Start is called before the first frame update
    [SerializeField] private GameObject gamePlay;
    [SerializeField] private GameObject gamePlay2;
    [SerializeField] private GameObject gamePlay3;
    [SerializeField] private GameObject player;
    public GameObject navi;
    public GameObject loseUI;
    public GameObject loseBoomUI;

    public GameObject winUI;
    public GameObject winFinalUI;
    [SerializeField]
    public GameObject PauseMenuUI;
    private bool gameIsPause = false;
    [SerializeField] private GameObject item;
    [SerializeField] private Sprite item2;
    [SerializeField] private Sprite item3;
    [SerializeField] private Transform character;
    private bool lose = false;

    public int gamePlayID = 0;

    public static GameManager Instance;

    public PodController podController;
    public CountDownTime countDown;
    RandomItem randomItem;
    private bool checkUIPause=true;
    public bool checkUIBoom = false;

    private void Awake()
    {
        Instance = this;
        Time.timeScale = 0;
    }
    public void gameLose()
    {
        checkUIPause = true;
        navi.SetActive(false);
        Time.timeScale = 0;
        //  lose = true;
        player.SetActive(false);
        gamePlay.SetActive(false);
        countDown.StartTime = false;
        gamePlay2.SetActive(false);
        gamePlay3.SetActive(false);
        destroyItem();
        PauseMenuUI.SetActive(false);
        loseUI.SetActive(true);
        AudioManager.Instance.PlaySFX(3);
    }
    public void gameLoseBoom()
    {
        checkUIPause = true;
        navi.SetActive(false);
        //Time.timeScale = 0;
        //  lose = true;
        player.SetActive(false);
        gamePlay.SetActive(false);
        countDown.StartTime = false;
        gamePlay2.SetActive(false);
        gamePlay3.SetActive(false);
        destroyItem();
        PauseMenuUI.SetActive(false);
        checkUIBoom = true;
        loseBoomUI.SetActive(true);
        AudioManager.Instance.PlaySFX(2);
    }
    public void GameWin()
    {
        checkUIPause = true;
        navi.SetActive(false);
        player.SetActive(false);
        countDown.StartTime = false;
        destroyItem();
        winUI.SetActive(true);
        AudioManager.Instance.PlaySFX(4);
    }
    public void NextLevel()
    {
        winUI.SetActive(false);
        player.SetActive(true);
        checkUIPause = false;
        if (gamePlayID == 2)
        {
            navi.SetActive(true);
            countDown.StartTime = true;
            countDown.timeStart = 60;
            gamePlay.SetActive(false);
            gamePlay2.SetActive(true);
        }
        else if (gamePlayID == 3)
        {
            navi.SetActive(true);
            countDown.StartTime = true;
            countDown.timeStart = 60;
            gamePlay2.SetActive(false);
            gamePlay3.SetActive(true);
        }
        else if (gamePlayID > 3)
        {
            checkUIPause = true;
            player.SetActive(false);
            winFinalUI.SetActive(true);
            destroyItem();
            navi.SetActive(false);
            AudioManager.Instance.PlaySFX(5);
        }
        podController.setDollar(0);
        podController.userPrice = 0;
        randomItem.CompareScore = 0;
        randomItem.GetComponent<RandomItem>();
    }
    public void GameStart()
    {
        checkUIPause = false;
        Time.timeScale = 1;
        // countDown.timeStart = 60;
        player.SetActive(true);
        gamePlay.SetActive(true);
        gamePlayID = 1;
    }
    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void destroyItem()
    {
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("coin"))
        {
            Destroy(o);
        }
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("garbage"))
        {
            Destroy(g);
        }
        foreach (GameObject d in GameObject.FindGameObjectsWithTag("diamond"))
        {
            Destroy(d);
        }
        foreach (GameObject b in GameObject.FindGameObjectsWithTag("boom"))
        {
            Destroy(b);
        }
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("gold"))
        {
            Destroy(go);
        }
    }
    public void CheckScore()
    {
        //500
        if (podController.userPrice >= 500 && (gamePlayID == 1)) // Level 1 
        {
            item.GetComponent<SpriteRenderer>().sprite = item2;
            item.transform.position = new Vector2(0, 1.5f);
            character.localPosition = new Vector2(-0.55f, 0.8f);
            item.GetComponent<MoveCharacter>().enabled = false;
            GameWin();

            gamePlayID = 2;
            return;
        }
        //700
        else if (podController.userPrice >= 800 && (gamePlayID == 2))
        {
            //Level 2
            item.GetComponent<SpriteRenderer>().sprite = item3;
            item.transform.position = new Vector2(-0.2f, 2.2f);
            character.localPosition = new Vector2(0.4f, 0.58f);
            item.GetComponent<MoveCharacter>().enabled = false;
            GameWin();
            gamePlayID = 3;
            return;
        }
        //1000
        else if (podController.userPrice >= 1100 && (gamePlayID == 3))
        { // Level 3 
            gamePlayID = 4;
            NextLevel();
            // navi.SetActive(false);
            return;
        }
    }
    public void PauseGame()
    {
        Time.timeScale = 0f;
        PauseMenuUI.SetActive(true);
        gameIsPause = true;
        podController._rotateSpeed = 0;
        podController.checkPause = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        PauseMenuUI.SetActive(false);
        gameIsPause = false;
        podController._rotateSpeed = 1;
        podController.checkPause = false;
    }
    void Update()
    {
        CheckScore();
        if (!checkUIPause)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                if (!gameIsPause)
                {

                    // gameIsPause = true;
                    GameManager.Instance.PauseGame();
                }
                else
                {
                    GameManager.Instance.ResumeGame();
                    //gameIsPause = false;
                }
            }
        }
       
    }

}

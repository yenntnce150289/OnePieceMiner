using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PodController : MonoBehaviour
{
    // Start is called before the first frame update
    public enum PodState
    {
        ROTATION,
        SHOOT,
        REWIND // khi keo len


    }

    #region Serialize
    [SerializeField]
    public int _rotateSpeed = 2;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private GameObject pricePopupPrefab;
    #endregion

    [SerializeField]
    GameObject player;

    public MoveCharacter move = new MoveCharacter();
    public bool checkPause;
    [SerializeField]
    PodState podState = PodState.ROTATION;
    private int _angle;
    private float _weightItem;
    private int _priceItem;
    public float userPrice = 0;
    private Transform _Rod;
    private int _flagRod = 0;
    public ScoreText scoreText;
    private Animator _mainAnimator;
    private RandomItem randomItem;
    public float timeStart;
    public bool countDown = false;
    void Start()
    {
        _mainAnimator = move.GetComponentInChildren<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "screen")
        {
            _flagRod = 3;
        }
        //cham vao vat -> huy trang thai rotaion
        else
        if (_flagRod == 0)
        {
            _flagRod = 1;
            //lay  slow down cua doi tuong Rod
            _Rod = col.transform;
            _Rod.SetParent(transform);
            if (_Rod.tag == "boom")
            {
                AudioManager.Instance.PlaySFX(2);
                AudioManager.Instance.StopBGM();
                countDown = true;
                // _Rod.tag = this.tag;
                _Rod.GetComponent<Boom>().Bang(_Rod.position);
                _flagRod = 0;
            }
            _weightItem = _Rod.GetComponent<RodController>().getWeightItem();
            _priceItem = _Rod.GetComponent<RodController>().getPriceItem();
        }
        podState = PodState.REWIND;
        _mainAnimator.Play("Rewind");

    }
    // Update is called once per frame
    void Update()
    {
        if (countDown)
        {
            timeStart -= 0.05f;
            if (timeStart <= 0)
            {
                //   Destroy(_Rod.gameObject);
                GameManager.Instance.gameLoseBoom();
            }
        }
        switch (podState)
        {
            case PodState.ROTATION:
                move.speedMove = 5;
                transform.position = new Vector3(player.transform.position.x - 0.17f, player.transform.position.y, player.transform.position.z);

                if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.Space))
                {
                    if (checkPause == false)
                    {
                        podState = PodState.SHOOT;
                        AudioManager.Instance.PlaySFX(1);
                        _mainAnimator.Play("Shoot");
                    }
                }
                _angle += _rotateSpeed; // tang goc quay
                // quay 1 goc tu -80 den 80
                if (_angle > 70 || _angle < -70)
                {
                    _rotateSpeed *= -1; // doi chieu quay
                }

                transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
                break;
            case PodState.SHOOT:

                move.speedMove = 0;
                transform.Translate(Vector3.down * _speed * Time.deltaTime);
                //neu nhan space -> chuyen sang shoot, xuong theo 
                if (_flagRod == 1)
                {
                    if (player.transform.position.x > transform.position.x || transform.position.y < player.transform.position.y)
                    {
                        podState = PodState.REWIND;

                    }
                }
                else if (player.transform.position.x > 8 || transform.position.y < -4)
                {
                    podState = PodState.REWIND;
                    _mainAnimator.Play("Rewind");

                }
                break;
            case PodState.REWIND:
                // nguoc lai
                transform.Translate(Vector3.up * (_speed - (_weightItem / 10)) * Time.deltaTime);

                if (Mathf.Floor(transform.position.y) == Mathf.Floor(player.transform.position.y))
                {

                    if (_flagRod == 1)
                    {
                        pricePopupPrefab.GetComponentInChildren<hideFloatPrice>().ShowPrice();
                        pricePopupPrefab.GetComponentInChildren<PricePopup>().ShowPriceItem(_priceItem);
                        // Debug.Log("DO TOI day r nef flagrod" + _priceItem);
                        userPrice += _priceItem;
                        Destroy(_Rod.gameObject);
                        _weightItem = 0; //reset
                        _flagRod = 0;
                        setDollar(userPrice);
                        AudioManager.Instance.PlaySFX(6);
                    }
                    else if(_flagRod ==3)
                    {
                        _flagRod = 0;
                    }
                    transform.position = player.transform.position;
                    podState = PodState.ROTATION;
                    _mainAnimator.Play("Rotation");
                }
                break;
        }
    }
    public void setDollar(float dollar)
    {
        scoreText.setText(string.Format("Score : {0} $", dollar));
    }
}

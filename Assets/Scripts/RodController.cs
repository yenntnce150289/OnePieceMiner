using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.PlayerSettings;

public class RodController : MonoBehaviour
{
    //[SerializeField]//change private => public 8 10 12 ----DO
    private string tagItem;
    //[SerializeField]
    private float weightItem; // can nang lam cham 
    //[SerializeField]
    private int priceItem; //tien se duoc cong vao
    
    void Start()
    {
        tagItem = this.tag;
        if (tagItem.Equals("gold"))
        {
            setItem(tagItem, 50, 200);
        } 
        else if (tagItem.Equals("diamond"))
        {
            setItem(tagItem, 20, 300);
        }
        else if (tagItem.Equals("garbage"))
        {
            setItem(tagItem, 90, 1);

        }
        else if (tagItem.Equals("coin"))
        {
            setItem(tagItem, 5, 50);
        }

    }

    public float getWeightItem()
    {
        return this.weightItem;
    }
    public int getPriceItem()
    {
        return this.priceItem;
    }
    public void Bang(Vector2 pos, bool flag = false)
    {
        var hits = Physics2D.CircleCastAll(pos, 3, Vector2.zero);
        foreach (var hit in hits)
        {
            if (hit.collider == null)
            {
                continue;
            }
            if (hit.transform.tag == Config.TAG_GOLD || hit.transform.tag == Config.TAG_DIAMOND || hit.transform.tag == Config.TAG_GARBAGE || hit.transform.tag == Config.TAG_COIN)
            {
                Debug.Log("dang o iff thu2 rod ne "+priceItem);
                Destroy(hit.transform.gameObject);
            }
            else if (hit.transform.tag == Config.TAG_BOOM)
            {
                Debug.Log("Boom noor ne");
                hit.transform.tag = Config.TAG_GOLD;
                hit.transform.GetComponent<RodController>().Bang(hit.point);
            }
        }
    }
    public void setItem(string tagName, float weightItem,int priceItem)
    {
        this.tagItem = tagName;
        this.weightItem = weightItem;
        this.priceItem = priceItem;
    }
}


using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PricePopup : MonoBehaviour
{
   
    public GameObject floatPrice;
  //  public MoveCharacter move = new MoveCharacter();
    Vector3 vec;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(HidePriceAfterShow(0.45f, floatPrice));
        Destroy(GameObject.Find("FloatingPrice(Clone)"), 0.5f);
    }
    public void ShowPriceItem(float price)
    {
        gameObject.SetActive(true);
        GameObject prefab = Instantiate(floatPrice,transform.localPosition, Quaternion.identity) as GameObject; 
        prefab.GetComponentInChildren<TextMeshPro>().text = " + " + price.ToString();
    }
    IEnumerator HidePriceAfterShow(float timeshow, GameObject game)
    {

        yield return new WaitForSeconds(timeshow);
        game.SetActive(false);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomItem : MonoBehaviour
{
    // public List<GameObject> spawnPool;
    public GameObject[] gameObject;
    public int[] quantityRandom;
    public GameObject quad;
    public float CompareScore;

    void Start()
    {
        // spawnObjects();
        randomObject(gameObject, quantityRandom);

    }
    //NEW
    //// <summary>
    // // Use this for initialization
    // // </summary>
    float ColliderHalfWidth;
    float ColliderHalfHeight;
    Vector2 min = new Vector2();
    Vector2 max = new Vector2();
    private void randomObject(GameObject[] game, int[] quantity)//new Test
    {

        destroySpawn();
        MeshCollider c = quad.GetComponent<MeshCollider>();
        float screenX, screenY;
        Vector2 pos;
        for (int i = 0; i < game.Length; i++)
        {
            string tagItem = game[i].GetComponent<RodController>().tag;

            for (int j = 0; j < quantity[i]; j++)
            {
                BoxCollider2D collider2D = game[i].GetComponent<BoxCollider2D>();
                ColliderHalfWidth = collider2D.size.x / 2;
                ColliderHalfHeight = collider2D.size.y / 2;
                //if (tagItem.Equals("gold"))
                //{
                //    CompareScore += 100 * quantity[i];
                //}
                //else if (tagItem.Equals("diamond"))
                //{
                //    CompareScore += 300 * quantity[i];
                //}
                //else if (tagItem.Equals("garbage"))
                //{
                //    CompareScore += 1 * quantity[i];
                //}
                //else if (tagItem.Equals("coin"))
                //{
                //    CompareScore += 50 * quantity[i];
                //}

                Vector3 location = new Vector3(Random.Range(c.bounds.min.x, c.bounds.max.x),
                                       Random.Range(c.bounds.min.y, c.bounds.max.y),
                                       -Camera.main.transform.position.z
                                      );
                //  Vector3 worldLocation = Camera.main.ScreenToWorldPoint(location);
                SetMinAndMax(location);

                // make sure we don't spawn into a collisio
                while (Physics2D.OverlapArea(min, max) != null)
                {
                    location.x = Random.Range(c.bounds.min.x, c.bounds.max.x);
                    location.y = Random.Range(c.bounds.min.y, c.bounds.max.y);
                    // worldLocation = Camera.main.ScreenToWorldPoint(location);
                    SetMinAndMax(location);
                }
                // create new bear if found collision-free location
                if (Physics2D.OverlapArea(min, max) == null)
                {
                 GameObject game1 = Instantiate(game[i], location, game[i].transform.rotation);
                    game1.transform.parent = transform;
                }
            }
        }
    }
    void SetMinAndMax(Vector3 location)
    {
        min.x = location.x - ColliderHalfWidth;
        min.y = location.y - ColliderHalfHeight;
        max.x = location.x + ColliderHalfWidth;
        max.y = location.y + ColliderHalfHeight;
    }


    public void destroySpawn()
    {
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("coin"))
        {
            Destroy(o);
        }
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("garbage"))
        {
            Destroy(g);
        }
        foreach (GameObject d in GameObject.FindGameObjectsWithTag("diamond"))
        {
            Destroy(d);
        }
        foreach (GameObject b in GameObject.FindGameObjectsWithTag("boom"))
        {
            Destroy(b);
        }
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("gold"))
        {
            Destroy(go);
        }
    }
}
